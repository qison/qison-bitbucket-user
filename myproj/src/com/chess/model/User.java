package com.chess.model;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author Saisudha
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "user")

public class User implements Serializable {
/**
* int id.
*/
@Id @GeneratedValue
    private int id;
/**
* String name.
*/
    private String name;
    /**
    * String email.
    */
    private String email;
    /**
     * String password.
     */
    private String password;
    /**
     * String mobile.
     */
    private Long mobile;
    /**
     * String usertype.
     */
    private String usertype;
    /**
     * user constructor.
     */
    public User() {
    }
/**
 * 
 * User Constructor
 * @param uname
 * @param mob
 * @param pswd
 * @param utype
 */
    public User(String uname, String mail, long mob, String pswd, String utype) {
        this.name = uname;
        this.email = mail;
        this.password = pswd;
        this.mobile = mob;
        this.usertype = utype;
    }

/**
 * @return user id
 */
    public final int getId() {
        return id;
    }
    /**
     * @param ide id to set
     */
    public final void setId(final int ide) {
        id = ide;
    }

/**
 * @return user name
 */
    public final String getName() {
        return name;
    }
    /**
     * @param uname
     * name to set
     */
    public final void setName(final String uname) {
        name = uname;
    }

/**
 * @return user email
 */
    public final String getEmail() {
        return email;
    }
    /**
     * @param mail
     * email to set
     */
    public final void setEmail(final String mail) {
        email = mail;
    }

/**
 * @return type of user
 */
    public final String getUserType() {
        return usertype;
    }
    /**
     * @param utype
     * type of user to set
     */
    public final void setUserType(final String utype) {
        usertype = utype;
    }

/**
 * @return user password
 */
    public final String getPassword() {
        return password;
    }
    /**
     * @param pswd password to set
     */
    public final void setPassword(final String pswd) {
        password = pswd;
    }

/**
 * @return user mobile
 */
    public final Long getMobile() {
        return mobile;
    }
    /**
     * @param mob mobile number to set
     */
    public final void setMobile(final Long mob) {
        mobile = mob;
    }
}


