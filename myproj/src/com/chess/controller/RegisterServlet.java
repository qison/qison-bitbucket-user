package com.chess.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.chess.model.User;
import com.chess.service.RegisterService;
import com.mysql.jdbc.StringUtils;

/**
 * Servlet implementation class RegisterServlet.
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
/**
* constructor.
*/
public RegisterServlet() {
super();
// TODO Auto-generated constructor stub
}

/**
* @param request
* @Param response
* @throws
*/
	public final void doPost(
			HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String email = request.getParameter("email");
		String firstname = request.getParameter("firstName");
		String lastname = request.getParameter("lastName");
		String password = request.getParameter("password");
		String confirmpassword =
				request.getParameter("confirmPassword");
		Long mobile = Long.
				parseLong(request.getParameter("mobile")
						);
		String usertype = request.getParameter("utype");
		if (StringUtils.isNullOrEmpty(email)) {
			String error = "Please fill out all the fields";
			request.setAttribute("message", error);
			request.getRequestDispatcher("/register1.jsp").
			forward(request, response);
		} else if (!password.equals(confirmpassword)) {
			String error = "Password and confirm"
					+ " password should be same";
			request.setAttribute("message", error);
			request.getRequestDispatcher("/register1.jsp").
			forward(request, response);
		} else {
			String name = firstname + " " + lastname;
			// System.out.println(name);
			User user = new User(
					name, email,
					mobile, password, usertype);
			try {
				RegisterService registerService =
						new RegisterService();
				boolean result = registerService.register(user);
				// out.println("<html>");
				// out.println("<head>");
				// out.println("<title>"
				// + "Registration Successful"
				// + "</title>");
				// out.println("</head>");
				// out.println("<body>");
				// out.println("<center>");
				if (result) {
					// out.println(
					// "<h1>Thanks for"
					// + " Registering "
					// + "with us :</h1>"
					// );
					String message = "Thanks for"
							+ " Registering"
							+ " with us."
							+ "Please Login..!!";
					request.getSession().
					setAttribute("message", message);
					response.sendRedirect("login1.jsp");
				} else {
					String error = "Registration failed due"
							+ " to duplication"
							+ " of email";
					request.getSession().
					setAttribute("message", error);
					response.sendRedirect("register1.jsp");
				}
//				out.println("</center>");
//				out.println("</body>");
//				out.println("</html>");
			} finally {
				out.close();
			}
		}
	}

}
