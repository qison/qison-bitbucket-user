package com.chess.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.chess.model.User;
import com.chess.service.LoginService;
import com.mysql.jdbc.StringUtils;

/**
 * Servlet implementation class LoginServlet.
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
/**
 *@param request
 *@param response
 */
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String email = request.getParameter("email");
		String password = request.getParameter("password");
		if (StringUtils.isNullOrEmpty(email)
				|| StringUtils.isNullOrEmpty(password)) {
			String error = "Please enter credentials";
			request.setAttribute("error", error);
			request.getRequestDispatcher("/login1.jsp").
			forward(request, response);
		} else {
			LoginService loginService = new LoginService();
			boolean result = loginService.
					authenticateUser(email, password);
			User user = loginService.getUserByEmail(email);
			if (result) {
				request.getSession().setAttribute("user", user);
				response.sendRedirect("home.jsp");

			} else {
				// response.sendRedirect("error.jsp");
				String error = "Your Login Was "
						+ "Unsuccessful"
						+ " - Please Try Again";
				request.getSession().
				setAttribute("error", error);
				response.sendRedirect("login1.jsp");
			}
		}
	}

}
