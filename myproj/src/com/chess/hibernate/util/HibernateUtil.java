package com.chess.hibernate.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
/**
 *
 * @author Saisudha
 *
 */
public class HibernateUtil {
/**
 *
 */
    private static final SessionFactory sessionFactory;

    static {
        try {
        // loads configuration and mappings
            Configuration configuration = new Configuration().
            configure("hibernate.cfg.xml");
            ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

            // builds a session factory from the service registry
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            //sessionFactory = new AnnotationConfiguration().
            //configure("hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
    }
/**
 *
 * @return session
 */
    public static Session openSession() {
        return sessionFactory.openSession();
    }
}

