package com.chess.service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.chess.hibernate.util.HibernateUtil;
import com.chess.service.CommonService;
import com.chess.model.User;
/**
 *
 * @author Saisudha
 *
 */
public class RegisterService {
/**
 * 
 * @param user
 * @return boolean value
 */
	public boolean register(User user) {
		Session session = HibernateUtil.openSession();
		if (isUserExists(user)) {
			return false;
		}
		String mdPass = new CommonService().md5(user.getPassword());
		if (mdPass != "") {
			user.setPassword(mdPass);
		}
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.saveOrUpdate(user);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return true;
	}
/**
* @param user To check whether user is available or not.
* @return user object
*/
	public final boolean isUserExists(final User user) {
		Session session = HibernateUtil.openSession();
		boolean result = false;
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query = session.
					createQuery("from User where email='"
			+ user.getEmail() + "'");
			User u = (User) query.uniqueResult();
			tx.commit();
			if (u != null) {
				result = true;
			}
		} catch (Exception ex) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return result;
	}

}









