<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.List"%>
    <%@ page import= "com.chess.service.LoginService" %>
    <%@ page import= "com.chess.model.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home page</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<center>
     <div id="container">
         <h1>Result Page</h1>
             <b>This is Sample Result Page</b><br/>
             
             <%
                 User user = (User) session.getAttribute("user"); %>
                 <b>Welcome <%= user.getName() %></b><br/>
             	<% if(user.getUserType().equals("trainer")){ %>
             		<b> Following are the list of trainees available</b>
             		<table>
             <thead>
                 <tr>
             		<td>Name</td>
             		<td>Email</td>
             		<td>Mobile</td>
             		</tr>
             </thead>
             <tbody>
                 <%
                     LoginService loginService = new LoginService();
                     List<User> list = loginService.getListOfUsers();
                     for (User u : list) {
                    	if(u.getUserType().equalsIgnoreCase("trainee")){ 
                 %>
                 <tr>
                     <td><%=u.getName()%></td>
                     <td><%=u.getEmail()%></td>
                     <td><%=u.getMobile()%></td>
                 </tr>
                 <%} } %>
             <tbody>
         </table>  
             	<% } %>
             <a href="logout.jsp">Logout</a>
          <br/>
     </div>
    </center>
</body>

</html>