/**
 * Login and registration page validations 
 */
	$("#loginFrm").validate({
        rules : {
        	email : {
                        required : true,
                        email : true
                },
   			password : {
   				required : true,
   				minlength : 5
   			}
        },
        messages : {
        	email : {
                        required : "Please enter email",
                },
            password : {
            		required : "Please enter password",
            		minlength : "Please enter minimum of 5"
            }
        }
  });
	
function checkLoginForm() {
    if(!$("#loginFrm").valid()) return false;  
    return true;
}

$('#regsubmit').on("click",function(){
//	e.preventDefault();
	if(!$("#signupFrm").valid()) return false;
	return true;
});

$('#signupFrm').validate({
	rules : {
		firstName : {
			required : true,
			minlength : 3
		},
		lastName : {
			required : true,
			minlength : 3
		},
		email : {
			required : true,
			email :true
		},
		password : {
			required : true,
			minlength : 5
		},
		confirmPassword: {
	        required: true,
	        minlength: 4,
	        equalTo: "#password"
	    },
	    mobile : {
	    	required : true,
	    	minlength : 10,
	    	maxlength : 10
	    },
	    utype : {
	    	required : true
	    }
	},
	messages : {
		firstName : {
			required : "Please enter firstname",
			minlength : "Name should be atleast 3 letters"
		},
		lastName : {
			required : "Please enter lastname",
			minlength : "Name should be atleast 3 letters"
		},
		email: {
			required : "Please enter email"
		},
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
        },
        confirmPassword: {
          required: "Please confirm the password again",
          minlength: "Your password must be at least 5 characters long",
          equalTo: "Please enter the same password as above"
        },
        mobile : {
        	required: "Please provide ur mobile number",
        	minlength : "Mobile number should be 10 digits"
        },
        utype : {
	    	required : "Please provide the type of user"
	    }

	},
	errorPlacement: function( label, element ) {
		if( element.attr( "name" ) === "utype" ) {
			element.parent().append(label).addClass("col-xs-5"); // this would append the label after all checkboxes
		} else {
			label.insertAfter( element ); // standard behaviour
		}
	}
		
});



