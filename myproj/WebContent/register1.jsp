<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Form</title>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">

<style type="text/css">
	.signupdiv{
    	margin: 20px;
    }
</style>	
</head>
<body>
<div class="signupdiv">
    <h2>Sign Up</h2>
    <% if (request.getSession().getAttribute("message") != null) { %>
    <p class= "error displayMsgs">
        <%=request.getSession().getAttribute("message")%>
    </p>
<% }
    if (request.getAttribute("message") != null) { %>
        <p class= "error displayMsgs">
        <%=request.getAttribute("message")%>
    </p>
    <% }
session.removeAttribute("message"); 
request.setAttribute("message",null); %>
    <form class="form-horizontal" name="signupFrm" method="POST" id="signupFrm" action="RegisterServlet">
    	<div class="form-group">
            <label class="control-label col-xs-3" for="firstName">First Name:</label>
            <div class="col-xs-5">
                <input type="text" class="form-control" id="firstName" name="firstName" maxlength="30" >
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3" for="lastName">Last Name:</label>
            <div class="col-xs-5">
                <input type="text" class="form-control" id="lastName" name="lastName" maxlength="30" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="email">Email:</label>
            <div class="col-xs-5">
                <input type="text" class="form-control" id="email" name="email" maxlength="100" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="password">Password:</label>
            <div class="col-xs-5">
                <input type="password" class="form-control" id="password" name="password" maxlength="50" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="confirmPassword">Confirm Password:</label>
            <div class="col-xs-5">
                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" maxlength="50" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="mobile">Mobile:</label>
            <div class="col-xs-5">
                <input type="text" class="form-control" id="mobile" name="mobile" maxlength="10" >
            </div>
        </div>
        <div class="form-group" id="radiotrainee">
            <label class="control-label col-xs-3">UserType:</label>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="utype" value="trainee"> Trainee
                </label>
            </div>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="utype" value="trainer"> Trainer
                </label>
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-9">
                <input type="submit" class="btn btn-primary" value="Submit" id="regsubmit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/index.js"></script>  
<script src="js/bootstrap.min.js"></script>
</body>
</html>