<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login Page</title>
<script type="text/javascript" src="js/index.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form method="post" name="loginFrm" action="loginservlet">
<div style="padding: 100px 0 0 250px;">
<div id="login-box">
<h2>Login Page</h2>
<% if (request.getSession().getAttribute("message") != null) { %>
    <p style="color:green">
        <%=request.getSession().getAttribute("message")%>
    </p>
    
<% }
 if (request.getSession().getAttribute("error") != null) { %>
<p style="color:red">
    <%=request.getSession().getAttribute("error")%>
</p>

<% } 
session.removeAttribute("message"); 
session.removeAttribute("error"); %>
<div id="login-box-name" style="margin-top:20px;">Email:</div>
<div id="login-box-field" style="margin-top:20px;">
<input name="email" class="form-login" title="Email" value="" size="30" maxlength="50" />
</div>
<div id="login-box-name">Password:</div>
<div id="login-box-field">
<input name="password" type="password" class="form-login" title="Password" value="" size="30" maxlength="48" />
</div>
<br />
<span class="login-box-options">
New User?  <a href="register.jsp" style="margin-left:30px;">Register Here</a>
</span>
<br />
<br />
<input style="margin-left:100px;" type="submit" value="submit" onclick="return checkLoginForm();"/>
</div>
</div>
</form>
</body>
</html>