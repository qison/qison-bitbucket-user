<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Form</title>
<script>
</script>
<script type="text/javascript" src="js/index.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<h2>Registration Form</h2>
<% if (request.getSession().getAttribute("message") != null) { %>
    <p style="color:red">
        <%=request.getSession().getAttribute("message")%>
    </p>
<% }
session.removeAttribute("message");
//request.setAttribute("message", null); %>
<form method="POST" name="regFrm" action="RegisterServlet" >
<table align="center" cellpadding = "10">
<tr>
<td>Full Name</td>
<td><input type="text" name="fullName" maxlength="30"/>
(max 30 characters a-z and A-Z)
</td>
</tr>
<tr>
<td>Email</td>
<td><input type="text" name="email" maxlength="100" /></td>
</tr>
<tr>
<td>Password</td>
<td><input type="text" name="password" maxlength="50" /></td>
</tr>
<tr>
<td>Mobile</td>
<td><input type="text" name="mobile" maxlength="10" /></td>
</tr>
<tr>
<td>UserType</td>
<td><input type="radio" name="utype" value="trainee">Trainee<input type="radio" name="utype" value="trainer">Trainer</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit" onclick = "return checkRegForm();">
<input type="reset" value="Reset">
</td>
</tr>
</table>
</form>
</body>

</html>